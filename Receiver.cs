﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSignatureSample
{
    class Receiver
    {
        public RSAParameters PublicKeyInfo { get; set; }
        public RSAParameters PrivateKeyInfo { get; set; }

        RSACryptoServiceProvider RSA = new RSACryptoServiceProvider();

        public Receiver()
        {
            PublicKeyInfo = RSA.ExportParameters(false);
            PrivateKeyInfo = RSA.ExportParameters(true);
        }

        // Hash and sign the data
        public bool VerifyHash(RSAParameters sendersPublicKey, byte[] signatureToVerify, byte[] encryptedData)
        {
            RSACryptoServiceProvider rsaCSP = new RSACryptoServiceProvider();
            SHA1Managed hash = new SHA1Managed();
            byte[] hashedData;
            rsaCSP.ImportParameters(sendersPublicKey);
            hashedData = hash.ComputeHash(encryptedData);
            return rsaCSP.VerifyHash(hashedData, CryptoConfig.MapNameToOID("SHA1"), signatureToVerify);
        }

        public string DecryptData(byte[] encryptedData, RSAParameters rsaParams)
        {
            byte[] fromEncrypt;
            RSACryptoServiceProvider rsaCSP = new RSACryptoServiceProvider();
            rsaCSP.ImportParameters(PrivateKeyInfo);
            fromEncrypt = rsaCSP.Decrypt(encryptedData, false);
            ASCIIEncoding enc = new ASCIIEncoding();
            return enc.GetString(fromEncrypt);
        }
    }
}
