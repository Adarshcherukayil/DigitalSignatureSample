﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace DigitalSignatureSample
{
    class Program
    {
        static void Main(string[] args)
        {
            Sender sender = new Sender();
            Receiver receiver = new Receiver();

            // Encrypt with public key
            byte[] encryptedData = sender.EncryptData(Encoding.ASCII.GetBytes("Sample Data"), receiver.PublicKeyInfo);
            
            // Hash with private key
            byte[] hashedSignature = sender.HashAndSign(encryptedData);

            bool b = receiver.VerifyHash(sender.PublicKeyInfo, hashedSignature, encryptedData);
            Console.WriteLine("Signature has been " + (b ? "verified" : "not verified") );
            if (b)
                Console.WriteLine("Decrypted Data: " + receiver.DecryptData(encryptedData, receiver.PrivateKeyInfo));
        }
    }
}
