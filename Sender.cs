﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSignatureSample
{
    class Sender
    {
        public RSAParameters PublicKeyInfo { get; set; }
        public RSAParameters PrivateKeyInfo { get; set; }

        RSACryptoServiceProvider RSA = new RSACryptoServiceProvider();

        public Sender()
        {
            PublicKeyInfo = RSA.ExportParameters(false);
            PrivateKeyInfo = RSA.ExportParameters(true);
        }


        // Hash and sign the data
        public byte[] HashAndSign(byte[] encryptedData)
        {
            RSACryptoServiceProvider rsaCSP = new RSACryptoServiceProvider();
            rsaCSP.ImportParameters(PrivateKeyInfo);
            SHA1Managed hash = new SHA1Managed();
            byte[] hashedData = hash.ComputeHash(encryptedData);
            return rsaCSP.SignHash(hashedData, CryptoConfig.MapNameToOID("SHA1"));
        }

        public byte[] EncryptData(byte[] dataToEncrypt, RSAParameters receiversPublicKey)
        {
            RSACryptoServiceProvider rsaCSP = new RSACryptoServiceProvider();
            rsaCSP.ImportParameters(receiversPublicKey);
            return rsaCSP.Encrypt(dataToEncrypt, false);
        }
    }
}
